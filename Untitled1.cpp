#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <math.h>

#define BUFSIZE 100

class Tab
{
  int** _tab;
  int _m;
  int _n;
public:
  Tab(int m, int n)
  {
    _m=m;
    _n=n;
    _tab = (int**) malloc(m*sizeof(int*));
    for(int i=0;i<m;i++)
      _tab[i]= (int*) malloc(n*sizeof(int));
  }
  ~Tab()
  {
    free(_tab);
  }
  void Radom(int x);  //wypelnia losowymi intami od 0 do x
  void print();       //wypisuje na std::cout
  int  max();         //zwraca najwieksza wartosc z tablicy
};

void Tab::Radom(int x)
{
  srand(time(NULL));
  for(int i=0;i<_m;i++)
   {
     for(int j=0;j<_n;j++)
       _tab[i][j]=rand()%x;
   }
}

void Tab::print()
{
  for(int i=0;i<_m;i++)
    {
      for(int j=0;j<_n;j++)
	std::cout << _tab[i][j] << " ";
      std::cout << std::endl;
    }
}

int Tab::max()
{
  int maks=0;
  for(int i=0;i<_m;i++)
    {
      for(int j=0;j<_n;j++)
	if (_tab[i][j]>maks)
	  maks=_tab[i][j];
    }
  return maks;
}

void save(char* filename, int* tab, int length)
{
  std::ofstream file;
  file.open(filename);
  if(file.is_open())
    {
      for(int i=0;i<length;i++)
	file << tab[i];
      file.close();
    }
  else
    std::cerr << "Zapisywanie do pliku nie bangla." << std::endl;
}

void load(char* filename, int* &tab, int & length)
{
  std::ifstream file;
  char buffer[BUFSIZE];
  length=0;
  file.open(filename);
  if(file.is_open())
    {
      file.get(buffer, BUFSIZE);
      length=file.gcount();
      file.close();
      //free(tab);
      tab = (int*) malloc(length*sizeof(int));
      for(int i=0;i<length;i++)
	  tab[i]=buffer[i]-'0'; 
    }
  else
    std::cerr << "Wczytywanie z  pliku nie bangla." << std::endl;
}

bool jestPal_i(std::string str)
{
  bool is=true;
  if(str.empty())
    return true;  
  for(int i=0; i<floor(str.length()/2);i++)
    {
     if(str.begin()[i]!=str.rbegin()[i])
       {
         is=false;
         break;
       }       
    }
  return is;
}

bool jestPal(std::string str)
{
	if(str.length()<=2)
	  return 1;
	if(*str.begin()==*str.rbegin())
	{
	  str.erase(str.begin());
	  str.erase(str.end()-1);
	  if(jestPal(str))
	    return true;
	  else
	    return false;
    }
    else
      return false;
}

int potega(int x,int p)
{	
	if(p==0)
	  return 1;
	if(p<0)
	  return 0;
  	return x*potega(x, p-1);
}

int silnia(int x)
{
	if(x==0)
	  return 1;
	if(x<0)
	  return 0;
	return x*silnia(x-1);
}

int main()
{
  int m=4;
  int n=5;
  Tab tab(m,n);
  int len;
  int* benini;
  std::string pal="benisineb";
  
  tab.Radom(10);
  tab.print();
  std::cout<<tab.max()<<std::endl;
  int benin[10];
  for(int i=0;i<10;i++)
    benin[i]=i;
  save("benis", benin, 10);
  load("benis", benini, len);
  save("benini", benini, len);
  for(int i=0;i<len;i++)
  std::cout << benini[i];
  std::cout << std::endl << jestPal(pal) << std::endl << jestPal_i(pal);
  std::cout << std::endl << potega(2,10);
  std::cout << std::endl << silnia(3);
  std::cin >> m;
  return 0;
}
