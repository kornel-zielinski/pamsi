#include <iostream>
#include <stdlib.h>

struct elem_list
{
  int val;
  elem_list* next;	
  elem_list() {next=0;}
};

class Lista
{
  elem_list* _ptr;
public:
  Lista() {_ptr = 0;}
	bool empty()  const {return _ptr==0 ? true:false;}
  int  length() const;
  void print()  const;
	void add(int);
  void del(unsigned int);
  int  operator [] (unsigned int) const;
  int& operator [] (unsigned int);	
  void nuke() {while(!empty()) del(0);}
};

int nic;

void Lista::add(int a)
{
  elem_list* nowy;
  elem_list* temp;
  nowy = new elem_list;
  nowy->val=a;
  nowy->next=0;
  if(empty())
    _ptr=nowy;
  else
  {
    temp=_ptr;
    while(temp->next!=0)
	    temp=temp->next;
    temp->next=nowy;
  } 
}

void Lista::del(unsigned int index)
{ 
  elem_list** prev=&_ptr;
  elem_list*  temp;
  if(!empty())
  {
	  temp=_ptr;
    while(index>0 && temp->next!=0)
    {
	    prev=&temp->next;
      temp=temp->next;
      index--;
	  }
    if(index==0)
    {
	    *prev=temp->next;
      delete temp;
	  }
    else
      std::cerr << "Blad indeksowania, lista za krotka\n";
  }
  else
    std::cerr << "Lista pusta\n";
}

int Lista::length() const
{
  int len=0;
  elem_list* temp=_ptr;
	while(temp!=0)
  {
	  temp=temp->next;
    len++;
  }
  return len;
}

void Lista::print() const
{
  elem_list* temp=_ptr;
  while(temp!=0)
  {
    std::cout << temp->val << "\n";
    temp=temp->next;
  }
}	

int Lista::operator [] (unsigned int index) const
{
	elem_list* temp=_ptr;
  if(!empty())
  {
    while(index>0 && temp->next!=0)
    {
	    temp=temp->next;
      index--;
    }
    if(index==0)
        return temp->val;
    else
      std::cerr << "Blad indeksowania, lista za krotka\n";
  }
  else
    std::cerr << "Lista pusta.\n";
  return 0;
} 
int& Lista::operator [] (unsigned int index)
{
	elem_list* temp=_ptr;
  if(!empty())
  {
    while(index>0 && temp->next!=0)
    {
	    temp=temp->next;
      index--;
    }
    if(index==0)
        return temp->val;
    else
      std::cerr << "Blad indeksowania, lista za krotka\n";
  }
  else
    std::cerr << "Lista pusta.\n";
  return nic;
} 

int main()
{
	int a=5;
  Lista osobista;
  std::cout << "Wypelniam liste liczbami z 3 na koncu bo tak\n";
  for(int i=0;i<10;i++)
    osobista.add(10*i+3);
  osobista.print();
  std::cout << "dlugosc listy: "  <<  osobista.length()  << '\n';
  std::cout << "\nUsuwam " << a << " element.\n\n";
  osobista.del(a);
  osobista.print();
  std::cout << "dlugosc listy: "  <<  osobista.length()  << '\n';
  std::cout << "Usuwam cala liste\n";
  osobista.nuke();
  osobista.print();
  std::cout << "dlugosc listy: "  <<  osobista.length()  << '\n';
  //osobista[a]=10;
  //std::cout << "vwartosc nr " << a << ": " << osobista[a] << '\n';
  //std::cout << "dlugosc listy: "  <<  osobista.length()  << '\n';
  std::cin >> a;
}
