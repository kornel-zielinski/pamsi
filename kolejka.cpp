#include <iostream>
#include <stdlib.h>

struct elem_list
{
  int val;
  elem_list* next;	
  elem_list() {next=0;}
};

class Lista
{
  elem_list* _ptr;
public:
  Lista() {_ptr = 0;}
	bool empty()  const {return _ptr==0 ? true:false;}
  int  length() const;
  void print()  const;
	void queue(int);
  int  dequeue();
  int  operator [] (unsigned int) const;
  int& operator [] (unsigned int);	
  void nuke() {while(!empty()) dequeue();}
};

int nic;

int Lista::length() const
{
  int len=0;
  elem_list* temp=_ptr;
	while(temp!=0)
  {
	  temp=temp->next;
    len++;
  }
  return len;
}

void Lista::print() const
{
  elem_list* temp=_ptr;
  while(temp!=0)
  {
    std::cout << temp->val << "\n";
    temp=temp->next;
  }
}	

void Lista::queue(int a)
{
  elem_list* nowy;
  elem_list* temp;
  nowy = new elem_list;
  nowy->val=a;
  nowy->next=0;
  if(empty())
    _ptr=nowy;
  else
  {
    temp=_ptr;
    while(temp->next!=0)
	    temp=temp->next;
    temp->next=nowy;
  } 
}

int Lista::dequeue()
{ 
  elem_list*  temp=_ptr;
  int wynik;
  if(!empty())
  {
    wynik=_ptr->val;
    _ptr=_ptr->next;
    delete temp;
  } 
  else
  {
    std::cerr << "Lista pusta\n";
    wynik=nic;
  }
  return wynik;
}


int Lista::operator [] (unsigned int index) const
{
	elem_list* temp=_ptr;
  if(!empty())
  {
    while(index>0 && temp->next!=0)
    {
	    temp=temp->next;
      index--;
    }
    if(index==0)
        return temp->val;
    else
      std::cerr << "Blad indeksowania, lista za krotka\n";
  }
  else
    std::cerr << "Lista pusta.\n";
  return 0;
} 
int& Lista::operator [] (unsigned int index)
{
	elem_list* temp=_ptr;
  if(!empty())
  {
    while(index>0 && temp->next!=0)
    {
	    temp=temp->next;
      index--;
    }
    if(index==0)
        return temp->val;
    else
      std::cerr << "Blad indeksowania, lista za krotka\n";
  }
  else
    std::cerr << "Lista pusta.\n";
  return nic;
} 


int main()
{
	Lista wiadro;
  for(int i=0;i<10;i++)
    wiadro.queue(i);
  wiadro.print();
  while(!wiadro.empty())
    std::cout << "Sciagnieto element: " << wiadro.dequeue() << '\n';
  wiadro.dequeue();
  return 0;	
}
